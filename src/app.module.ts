import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import config from 'src/config';


@Module({
  imports: [ArticlesModule, MongooseModule.forRoot(config.mongoUri, {useNewUrlParser: true, useFindAndModify: false}), UsersModule, AuthModule
  
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
