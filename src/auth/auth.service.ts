import { Injectable, NotAcceptableException, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ReadUserDto } from 'src/users/dto/read-user.dto';
import { User } from 'src/users/interfaces/user.interface';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
           constructor(private readonly userService: UsersService, private readonly jwtService: JwtService) {}
    async validateUser(username: string, pass: string): Promise<any> {
            const user = await this.userService.findOne(username);
            if(user && user.password === pass) {
                const { password, ...result } = user; 
                return result;
            }
            
            return null;
    }

    async login(readUserDto: ReadUserDto) {
       const foundUser = await this.userService.findOne(readUserDto.email);
       if (!foundUser) {
           throw new NotAcceptableException();
       }

       if (foundUser.password !== readUserDto.password) {
           throw new NotFoundException();
       }

       //return foundUser;

       const payload = {
           createdAt: new Date().toISOString(),
           sub:  foundUser._id,
           role: ''
       }

       if (foundUser.email === 'isow611@gmail.com') {
           payload.role = 'admin';
       } else {
           payload.role = 'user';
       }

       return {
           access_token: this.jwtService.sign(payload)
       }
    }
}
